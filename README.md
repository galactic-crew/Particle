# Particle

to install dependencies: `pip install -r (path to requirements.txt)`
you will need to auth with GitHub/GitLab in `config_template.py`
to build: `python3 (path to builder.py)`
if you just want to set up CFW see https://cfw.rf.gd